package CZN.service;

import org.springframework.stereotype.Component;
import CZN.enums.Language;
import CZN.model.standart.LanguageUser;
import CZN.repository.LanguageUserRepo;
import CZN.repository.TelegramBotRepositoryProvider;
import CZN.repository.UsersRepo;

import java.util.HashMap;
import java.util.Map;

@Component
public class LanguageService {

    private static  Map<Long, Language>     languageMap         = new HashMap<>();
    private static  LanguageUserRepo        languageUserRepo    = TelegramBotRepositoryProvider.getLanguageUserRepo();
    private static  UsersRepo               usersRepo           = TelegramBotRepositoryProvider.getUsersRepo();

    public  static  Language    getLanguage(long chatId) {
        Language language = languageMap.get(chatId);
        if (language == null) {
            LanguageUser languageUser = languageUserRepo.getByChatId(chatId);
            if (languageUser != null) {
                language = Language.getById(languageUser.getLanguageId());
                languageMap.put(chatId, language);
            }
        }
        return language;
    }

    public  static  void        setLanguage(long chatId, Language language) {
        languageMap.put(chatId, language);
        LanguageUser languageUser = languageUserRepo.getByChatId(chatId);
        if (languageUser == null) {
            languageUserRepo.save(new LanguageUser().setChatId(chatId).setLanguageId(language.getId()));
        } else {
            languageUserRepo.save(languageUser.setLanguageId(language.getId()));
        }
    }
}
