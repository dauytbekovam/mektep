package CZN.command;

import CZN.command.impl.*;
import CZN.exceptions.NotRealizedMethodException;

public class CommandFactory {

    public static Command getCommand(long id) {
        Command result = getCommandWithoutReflection((int) id);
        if (result == null) throw new NotRealizedMethodException("Not realized for type: " + id);
        return result;
    }
    private static Command getCommandWithoutReflection(int id) {
        switch (id) {
            case 1:
                return new id001_ShowInfo();
            case 2:
                return new id002_Registration();
            case 3:
                return new id003_SelectionLanguage();
            case 4:
                return new id004_FileOrPhoto();
            case 6:
                return new id006_EditAdmin();
            case 5:
                return new id005_ShowAdminInfo();

        }
        return null;
    }
}
