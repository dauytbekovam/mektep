package CZN.command.impl;

import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import CZN.command.Command;
import CZN.service.RegistrationService;

public class id002_Registration extends Command {

    private RegistrationService registrationService = new RegistrationService();

    @Override
    public boolean execute() throws TelegramApiException {
        deleteMessage(updateMessageId);
        if (!isRegistered()) {
            if (!registrationService.isRegistration(update, botUtils)) {
                return COMEBACK;
            } else {
                usersRepo.save(registrationService.getUser());
                sendMessageWithAddition();
            }
        } else {
            sendMessageWithAddition();
        }
        return EXIT;
    }
}
